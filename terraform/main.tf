provider "aws" {
  profile = "default"
  region  = "ap-southeast-1"
}

terraform {
  backend "s3" {
    bucket = "hgphuc.terraform-state"
    key    = "terraform.tfstate"
    region = "ap-southeast-1"
  }
}

locals {
  tags = {
    terraform   = "true"
    Environment = var.env
  }
}

### VPC
module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "5.6.0"

  name = "${var.env}-${var.vpc_name}"
  cidr = var.vpc_cidr

  azs = var.vpc_azs

  public_subnets      = var.vpc_public_subnets
  private_subnets     = var.vpc_private_subnets
  enable_nat_gateway  = true
  single_nat_gateway  = true
  reuse_nat_ips       = true
  external_nat_ip_ids = aws_eip.nat[*].id

  enable_dns_hostnames = true
  enable_dhcp_options  = true

  map_public_ip_on_launch = false

  default_security_group_egress = [{
    cidr_blocks = "0.0.0.0/0"
    }
  ]

  tags = local.tags
}

resource "aws_eip" "nat" {
  count  = length(var.vpc_azs)
  domain = "vpc"
}

### EKS
module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "~> 20.0"

  cluster_name    = "${var.env}-littlelives"
  cluster_version = "1.29"

  cluster_endpoint_public_access = true

  cluster_addons = {
    coredns = {
      most_recent = true
    }
    kube-proxy = {
      most_recent = true
    }
    vpc-cni = {
      most_recent = true
    }
    aws-ebs-csi-driver = {
      most_recent = true
    }
  }

  vpc_id     = module.vpc.vpc_id
  subnet_ids = module.vpc.private_subnets

  eks_managed_node_group_defaults = {
    instance_types = ["m5.large"]
  }

  eks_managed_node_groups = {
    app = {
      min_size     = 1
      max_size     = 3
      desired_size = 1

      instance_types = ["t3.large"]
      capacity_type  = "ON_DEMAND"
    }

    db = {
      min_size     = 1
      max_size     = 1
      desired_size = 1

      instance_types = ["t3.large"]
      capacity_type  = "ON_DEMAND"
    }
  }

  enable_cluster_creator_admin_permissions = true

  tags = local.tags
}


### Certificate
module "acm" {
  source  = "terraform-aws-modules/acm/aws"
  version = "~> 4.0"

  zone_id             = module.zones.route53_zone_zone_id[var.hosted_zone]
  domain_name         = var.hosted_zone
  wait_for_validation = false
  validation_method   = "DNS"

  tags = local.tags
}

### Route53
module "zones" {
  source  = "terraform-aws-modules/route53/aws//modules/zones"
  version = "~> 2.0"

  zones = {
    "${var.hosted_zone}" = {
      tags = local.tags
    }
  }

  tags = local.tags
}

module "rds_security_group" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 5.0"

  name        = "${var.env}-db"
  description = "Cloud db security group"
  vpc_id      = module.vpc.vpc_id

  ingress_with_cidr_blocks = [
    {
      rule        = "postgresql-tcp"
      cidr_blocks = "0.0.0.0/0"
    }
  ]

  tags = local.tags
}

module "rds" {
  source = "terraform-aws-modules/rds/aws"

  identifier             = "${var.env}-db"
  vpc_security_group_ids = [module.security_group.security_group_id]
  instance_class         = var.rds_db_instance_class
  username               = "dozer"
  allocated_storage      = var.rds_db_allocated_storage
  publicly_accessible    = true
  db_name                = "${var.env}_realworld"
  engine                 = "mysql"
  engine_version         = "8"
  family                 = "mysql8.0"
  port                   = "3306"
  create_db_subnet_group = true
  subnet_ids             = module.vpc.private_subnets

  # managed by secret manager
  manage_master_user_password = true
  deletion_protection         = true

  tags = local.tags
}

# module "records" {
#   source  = "terraform-aws-modules/route53/aws//modules/records"
#   version = "~> 2.0"

#   zone_name = module.zones.route53_zone_name[var.hosted_zone]

#   records = [
#     {
#       name    = ""
#       type    = "CNAME"
#       ttl     = "300"
#       records = [module.alb.lb_dns_name]
#     },
#   ]

#   depends_on = [module.zones]
# }

