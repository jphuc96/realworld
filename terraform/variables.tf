variable "env" {
  type = string
}

variable "vpc_name" {
  type = string
}

variable "vpc_cidr" {
  type = string
}

variable "vpc_azs" {
  type = list(string)
}

variable "vpc_public_subnets" {
  type = list(string)
}

variable "vpc_private_subnets" {
  type = list(string)
}

variable "hosted_zone" {
  type = string
}

variable "rds_db_instance_class" {
  type = string
}

variable "rds_db_allocated_storage" {
  type = number
}
