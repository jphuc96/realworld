Before going live, we need some components to be installed by terraform (./terraform directory):

- VPC with 3 public subnets and 3 private subnets
- RDS Mysql instance
- EKS Cluster
- Route53 hosted zone
- ACM certificate

The Gitlab CI pipeline will do:

- Build the code
- Run the tests
- Run the security checks
- Build docker image
- Push new image tag to gitops repository at https://gitlab.com/jphuc96/realworld-gitops

To do Gitops we need to install ArgoCD in the EKS cluster and create an application to sync the gitoops repository to the cluster.

Diagram:

![image](diagram.png)

To ensure the high availability and reliability of the application, we need to take care of the following:

- The RDS has read replicas and backups
- The EKS is deplyed in 3 AZs
- The Kubernetes deployment has as least 3 replicas and spans across nodes in different AZs using node affinity, anti-affinity, and pod disruption budget
- Horizontal Pod Autoscaler is enabled
- Setup the liveness and readiness
- Setup CloudWatch monitoring and logging and alerting
